<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage togo
 * @since Twenty Ten 1.0
 */

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Stichting kinderhulp Togo</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href="<?php bloginfo('template_directory'); ?>/Images/Opmaak/favicon.ico" rel="shortcut icon">

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>


</head>
<body>
<div id="Container">
  <div id="TopBanner"><?php _e("
<!--:nl--><img src='http://localhost/kinderhulptogo/cms/wp-content/themes/togo/Images/Topbanners_1/Banner_Logo_NED.png' alt='Nederlands' /><!--:-->
<!--:fr--><img src='http://localhost/kinderhulptogo/cms/wp-content/themes/togo/Images/Topbanners_1/Banner_Logo_Frans.jpg' alt='Frans' />
<!--:en--><img src='http://localhost/kinderhulptogo/cms/wp-content/themes/togo/Images/Topbanners_1/Banner_Logo_English.jpg' alt='English' />
<!--:de--><img src='http://localhost/kinderhulptogo/cms/wp-content/themes/togo/Images/Topbanners_1/Banner_Logo_Deutsch.jpg' alt='Deutsch' />
<!--:-->"); ?></div>
  <div id="TopBanner2"><img src="http://localhost/kinderhulptogo/cms/wp-content/themes/togo/Images/Topbanners_2/Banner_<?php echo(rand(1,35)); ?>.jpg" width="903" height="320" alt="Banner" /></div>

  
  <div id="LoGo"></div>

  <div id="AchtergrondMidden">
  <div id="Linkerfoto"><img src="http://localhost/kinderhulptogo/cms/wp-content/themes/togo/Images/Randomlinks/Fotolinks<?php echo(rand(1,8)); ?>.jpg" alt="Linkerfoto" /> </div>
<div id="Middenblok">
			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>
<p>&nbsp;</p>

      <div id="Tekstsluiter"> </div>
    </div>
    <div id="VertNavigatie">
  <div id="primary" class="widget-area" role="complementary">

<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

			<li id="search" class="widget-container widget_search">
				<?php get_search_form(); ?>
			</li>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"><?php _e( 'Archives', 'togo' ); ?></h3>
				<ul>
					<?php wp_get_archives( 'type=monthly' ); ?>
				</ul>
			</li>

			<li id="meta" class="widget-container">
				<h3 class="widget-title"><?php _e( 'Meta', 'togo' ); ?></h3>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</li>

		<?php endif; // end primary widget area ?>

	  </div>
</div>
<div id="Footer">
	<?php if ( is_active_sidebar( 'first-footer-widget-area' ) ) : ?>
	  <div id="first" class="widget-area">

						<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>

				</div><!-- #first .widget-area -->
    <?php endif; ?>
    </div>
  </div>
  <div id="TabjesBovenaan">
  <?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">

				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>

		</div><!-- #secondary .widget-area -->

<?php endif; ?>
  
</div>
  <div id="DoorzichtigeFolie">
<div id="Topnavigatie">				<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?></div>
  </div>

  </div>

</body>
</html>
