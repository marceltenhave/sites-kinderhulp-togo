<?php
/**
 * Template Name: HomeChildren
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage twentyten
 * @since Twenty Ten 1.0
 */

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<?php bloginfo('template_directory'); ?>/Images/Opmaak/favicon.ico" rel="shortcut icon">
<title>Stichting Kinderhulp Togo</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />


</head>
<body>  
<div id="ContainerHomepage">
  <div>
    <img src="<?php bloginfo('template_directory'); ?>/Images/Topbanners_1/Achtegrond055.jpg" alt="Stichting kinderhulp Togo - Association soutiens enfants Togo" width="1680" height="1050" border="0" usemap="#Map">
    <map name="Map">
    <area shape="poly" coords="606,448,606,450" href="#">
    <area shape="rect" coords="518,205,1199,543"  href="<?php echo site_url(); ?>/home/?lang=nl" alt="Hier bezoekt u het Nederlandse gedeelte van de site">
    <area shape="rect" coords="517,548,1200,881"  href="<?php echo site_url(); ?>/home/?lang=fr" alt="Visiter Association Soutien Enfants Togo">
    <area shape="rect" coords="1201,205,1419,545" href="<?php echo site_url(); ?>/home/?lang=de" alt="Besugen sie die Deutsche Website">
    <area shape="rect" coords="1202,549,1419,880" href="<?php echo site_url(); ?>/home/?lang=en" alt="English site">
    </map>
  </div>
</div>
<?php
   /* Always have wp_footer() just before the closing </body>
    * tag of your theme, or you will break many plugins, which
    * generally use this hook to reference JavaScript files.
    */

    wp_footer();
?>
</body>
</html>
