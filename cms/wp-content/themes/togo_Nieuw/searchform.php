<?php
/**
 * The search form
 *
 */
?>

<form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
<input class="text" type="text" value=" " name="s" id="s" />
<input type="hidden" id="lang" name="lang" value="<?php echo qtranxf_getLanguage(); ?>"/>
<input type="submit" class="submit button" name="submit" value="<?php _e('Search');?>" />
</form>